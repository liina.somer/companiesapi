package ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.model.Dog;
import ee.valiit.companiesapi.model.OperationResult;
import ee.valiit.companiesapi.repository.CompaniesRepository;
import ee.valiit.companiesapi.service.CompaniesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")

public class CompaniesController {
    @Autowired
    private CompaniesRepository companiesRepository;

    @Autowired
    private CompaniesService companiesService;

    @GetMapping("/hello/{name}")
    public String getHelloWorld(@PathVariable("name") String name) {
        return "Hello, " + name + "!";
    }
    @GetMapping("/dog")
    public Dog[] getDog(){
        Dog myDog = new Dog();
        myDog.setName("Muki");
        myDog.setTailLength(56);

        Dog myDog2 = new Dog();
        myDog2.setName("Rex");
        myDog2.setTailLength(123);
        return new Dog[] {myDog, myDog2};
    }
//get- all companies
    @GetMapping("/all")
    public List<Company> getCompanies() {
        return companiesRepository.getAllCompanies();
    }
    //Backend
    //get-single company bi id
    @GetMapping("/{companyId}")
    public Company getSingleCompany(@PathVariable("companyId") int id){
        return companiesRepository.getCompany(id);
    }

    //POST-by name,parem kui GET, kuna ei taha kogu nime kirjutada
    @PostMapping("/all")
    public List<Company> getCompaniesByName(@RequestParam(value = "companyName", defaultValue = "") String name) {
        return companiesRepository.getAllCompaniesByName(name);
    }

//DELETE- delete single company by
    @DeleteMapping("/{companyId}")
    public ResponseEntity<OperationResult> deleteCompany(@PathVariable("companyId") int id){
        OperationResult result = companiesService.deleteCompany(id);
        if (!result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else{
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        }
    }

    //POST- add company
    @PostMapping("/add")
    public ResponseEntity<OperationResult> addCompany(@RequestBody Company company){
        OperationResult result = companiesService.addCompany(company);
        if (!result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else{
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        }
    }


    //POST/PUT- update company
    @PutMapping("/update")
    public ResponseEntity<OperationResult> updateCompany(@RequestBody Company company){
       OperationResult result = companiesService.updateCompany(company);
        if (!result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        } else{
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        }
    }
    //CORS- what´s that?

    //UI for REST API
}
