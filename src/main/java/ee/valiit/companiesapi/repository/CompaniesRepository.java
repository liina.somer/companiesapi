package ee.valiit.companiesapi.repository;

import ee.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository

public class CompaniesRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Company> getAllCompanies() {
        return jdbcTemplate.query("select * from company", (row, number) -> {
            return new Company(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("logo"),
                    row.getString("established"),
                    row.getInt("employees")
            );
        });
    }

    public List<Company> getAllCompaniesByName(String name) {
        return jdbcTemplate.query("select * from company where `name` like ?",
                new Object[]{"%" + name + "%"},
                (row, number) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo"),
                            row.getString("established"),
                            row.getInt("employees")
                    );
                });
    }

    public Company getCompany(int companyId) {
        List<Company> companies = jdbcTemplate.query(
                "select * from company where id = ?",
                new Object[]{companyId},
                (row, number) -> {
                    return new Company(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo"),
                            row.getString("established"),
                            row.getInt("employees")
                    );
                });
        return companies.size() > 0 ? companies.get(0) : null;
    }

    public boolean companyExists(int companyId) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(*) from company where id = ?",
                new Object[] { companyId },
                Integer.class
        );
        return count != null && count > 0;
    }

    public void deleteCompany(int companyId) {
        jdbcTemplate.update("delete from company where id = ?", companyId);

    }

    public int addCompany(Company company) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into company (`name`, `logo`, `established`, `employees`) values (?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, company.getName());
            ps.setString(2, company.getLogo());
            ps.setString(3, company.getEstablished());
            ps.setInt(4, company.getEmployees());
            return ps;
        }, keyHolder);
        return keyHolder.getKey().intValue();
//        jdbcTemplate.update("insert into company (`name`, `logo`, `established`, `employees`) values(?, ?, ?, ?)",
//                company.getName(), company.getLogo(), company.getEstablished(), company.getEmployees()
//        );

    }
    public void updateCompany(Company company){
        jdbcTemplate.update(
                "update company set `name` = ?, `logo` = ?, `established` = ?, `employees` = ? where `id` = ?",
                company.getName(), company.getLogo(), company.getEstablished(), company.getEmployees(), company.getId()
        );
    }
}

