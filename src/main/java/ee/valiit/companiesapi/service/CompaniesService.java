package ee.valiit.companiesapi.service;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.model.OperationResult;
import ee.valiit.companiesapi.repository.CompaniesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service

public class CompaniesService {

    @Autowired
    private CompaniesRepository companiesRepository;

    public OperationResult deleteCompany(int companyId){
        try{
        Assert.isTrue(companyId > 0, "Company ID not specified!");
        Assert.isTrue(companiesRepository.companyExists(companyId), "The specified company does not exist!");
        companiesRepository.deleteCompany(companyId);
        return new OperationResult(true, "OK", companyId);
    } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return new OperationResult(false, e.getMessage(), null);
        }
    }

    public OperationResult addCompany(Company company) {
        try {
            Assert.isTrue(company != null, "Company data does not exist!");
            Assert.isTrue(company.getName() != null && !company.getName().equals(""), "Company name not specified!");
            Assert.isTrue(company.getLogo() != null && !company.getLogo().equals(""), "Company logo not specified!");
            Assert.isTrue(company.getEstablished() != null && !company.getEstablished().equals(""), "Company established date not specified!");
            List<Company> allCompaniesByName = companiesRepository.getAllCompaniesByName(company.getName());
            Assert.isTrue(allCompaniesByName.size() == 0, "The company with specified name already exists!");
            int companyId = companiesRepository.addCompany(company);
            return new OperationResult(true, "OK", companyId);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
            return new OperationResult(false, e.getMessage(), null);
        }
    }

    public OperationResult updateCompany(Company company) {
        try {
            Assert.isTrue(company != null, "Company data does not exist!");
            Assert.isTrue(company.getName() != null && !company.getName().equals(""), "Company name not specified!");
            Assert.isTrue(company.getLogo() != null && !company.getLogo().equals(""), "Company logo not specified!");
            Assert.isTrue(company.getEstablished() != null && !company.getEstablished().equals(""), "Company established date not specified!");
            List<Company> allCompaniesByName = companiesRepository.getAllCompaniesByName(company.getName());
            Assert.isTrue(allCompaniesByName.size() == 0 || allCompaniesByName.get(0).getId() == company.getId(),
                    "Another company with specified name already exists");
            companiesRepository.updateCompany(company);
            return new OperationResult(true, "OK", company.getId());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return new OperationResult(false, e.getMessage(), null);
        }
    }
}
